package org.alo.issues;

import javax.servlet.ServletException;

import org.openbravo.erpCommon.ad_callouts.SimpleCallout;

public class SetCancelledOrder extends SimpleCallout {

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    String newOrderId = info.getStringParameter("inpdescription");
    info.addResult("inpcancelledorderId", newOrderId);
    info.addResult("MESSAGE", "cancel order " + newOrderId);

  }

}
